﻿using System.Data;

namespace SoulShat
{
    public static class ShartHelper
    {
        public static void PrintTable(DataTable table)
        {
            
            foreach (DataColumn column in table.Columns)
            {
                Console.Write("| ");
                Console.Write(column.ColumnName);
            }
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine("");
                foreach (object obj in row.ItemArray)
                {
                    string cell = obj.ToString();
                    Console.Write("| ");
                    Console.Write(cell);
                }
            }
            Console.WriteLine("");
            Console.WriteLine("------------");
            Console.WriteLine("");
        }
    }
}