﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using Soulseek;
using Soulseek.Diagnostics;

namespace SoulShat // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        private static string OutputDirectory { get; set; }
        
        static async Task Main(string[] args)
        {
            OutputDirectory = "./SSS";
            var table = CSVtoDataTable("ivory_tower.csv");
            //ShartHelper.PrintTable(table);
            foreach(DataRow row in table.Rows)
            {
                Console.WriteLine(row["Track Name"]);
            }
            var options = new SoulseekClientOptions(
                minimumDiagnosticLevel: DiagnosticLevel.None,
                peerConnectionOptions: new ConnectionOptions(connectTimeout: 30000, inactivityTimeout: 15000),
                transferConnectionOptions: new ConnectionOptions(connectTimeout: 30000, inactivityTimeout: 15000)
            );
            var client = new SoulseekClient(options);
            await client.ConnectAsync("JohnGalt420", "Password");

            var responses = await SearchAsync(client, "lemon jelly a man like me");

            
            responses = responses
                .OrderByDescending(r => r.FreeUploadSlots)
                .ThenByDescending(r => r.UploadSpeed)
                .ThenByDescending(r => r.Files.OrderBy(f => f.BitRate));
            
            var response = responses.ToList()[0];
            var directories = response.Files
                .GroupBy(f => Path.GetDirectoryName(f.Filename))
                .ToDictionary(g => g.Key, g => g.ToList());
            
            ListResponseFiles(directories);
            await DownloadFilesAsync(client, response.Username, response.Files.Select(f => f.Filename).ToList())
                .ConfigureAwait(false);
            Console.ReadKey();
        }

        private static async Task<IEnumerable<SearchResponse>> SearchAsync(SoulseekClient client, string searchText, int minimumFileCount = 0)
        {
            var totalResponses = 0;
            var totalFiles = 0;
            var state = SearchStates.None;

            Console.Write($"Searching for '{searchText}'");


            var (search, responses) = await client.SearchAsync(SearchQuery.FromText(searchText),
                options: new SearchOptions(
                    filterResponses: true,
                    minimumResponseFileCount: minimumFileCount,
                    searchTimeout: 10000,
                    stateChanged: (e) => state = e.Search.State,
                    responseReceived: (e) =>
                    {
                        totalResponses++;
                        totalFiles += e.Response.FileCount;
                    },
                    fileFilter: (file) => Path.GetExtension(file.Filename) == ".mp3"));

            Console.Write($"Search complete. found {totalFiles} files from {totalResponses} users");

            return responses;
        }
        private static void ListResponseFiles(Dictionary<string, List<Soulseek.File>> directories)
        {
            for (int i = 0; i < directories.Count; i++)
            {
                var key = directories.Keys.ToList()[i];
                Console.WriteLine($"\n{(i + 1)}.  {key}\n");

                var longest = directories[key].Max(f => Path.GetFileName(f.Filename).Length);

                foreach (var file in directories[key])
                {
                    var filename = file.Filename;
                    Console.WriteLine($"    {Path.GetFileName(filename).PadRight(longest)}  {file.Size,7}bytes  {$"{file.BitRate}kbps",9}  {TimeSpan.FromSeconds(file.Length ?? 0),7:m\\:ss}");
                }
            }
        }
        private static async Task DownloadFilesAsync(SoulseekClient client, string username, List<string> files)
        {
            var index = 0;

            var tasks = files.Select(async file =>
            {
                try
                {
                    var path = $"{OutputDirectory}{Path.DirectorySeparatorChar}{Path.GetDirectoryName(file).Replace(Path.GetDirectoryName(Path.GetDirectoryName(file)), "")}";
                    var filename = Path.Combine(path, Path.GetFileName(file));
                    
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    var transfer = await client.DownloadAsync(username, file, filename, startOffset: 0, token: index++, options: new TransferOptions(stateChanged: (e) =>
                    {
                        var key = (e.Transfer.Username, e.Transfer.Filename, e.Transfer.Token);
                        
                    }, progressUpdated: (e) =>
                    {
                        var key = (e.Transfer.Username, e.Transfer.Filename, e.Transfer.Token);


                        var fn = Path.GetFileName(e.Transfer.Filename.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar));

                        var size = $"{e.Transfer.BytesTransferred}/{e.Transfer.Size}";
                        var percent = $"({e.Transfer.PercentComplete,3:N0}%)";
                        

                        Console.Write($"\r  {fn}  {size}  {percent} {e.Transfer.AverageSpeed}bytes/s");
 
                    })).ConfigureAwait(false);
                    
                    file = file.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);


                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error downloading {file}: {ex.Message}");
                }
            });

            await Task.WhenAll(tasks).ConfigureAwait(false);
        }
        static DataTable CSVtoDataTable(string strFilePath)  
        {  
            StreamReader sr = new StreamReader(strFilePath);  
            string[] headers = sr.ReadLine().Split(',');   
            DataTable dt = new DataTable();  
            foreach (string header in headers)  
            {  
                dt.Columns.Add(header.Replace("\"", ""));  
            }  
            while (!sr.EndOfStream)  
            {  
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");  
                DataRow dr = dt.NewRow();  
                for (int i = 0; i < headers.Length; i++)  
                {  
                    dr[i] = rows[i].Replace("\"", "");  
                }  
                dt.Rows.Add(dr);  
            }  
            return dt;  
        }   

    }
}